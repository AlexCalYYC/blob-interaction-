/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import java.util.ArrayList;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author alex
 */
public class Group implements Groupable{
    ArrayList <Groupable> groups;
    BoundingBox bbox;
    int zOrder;
   
    public Group(ArrayList<Groupable> gs){
        groups = new ArrayList<>();
        gs.forEach(g->groups.add(g));
        bbox = new BoundingBox();
        recalculateBounds();
    }
    
    private void recalculateBounds(){
        bbox.left=Double.MAX_VALUE;
        bbox.top=Double.MAX_VALUE;
        bbox.right=0;
        bbox.bottom=0;
        
        groups.forEach(g->{
        bbox.left=Math.min(bbox.left, g.getLeft());
        bbox.right=Math.max(bbox.right, g.getRight());
        bbox.top=Math.min(bbox.top, g.getTop());
        bbox.bottom=Math.max(bbox.bottom, g.getBottom());
        });
    }
    
    
    public double getLeft(){
        return bbox.left;
    }
    public double getRight(){
        return bbox.right;
    }
    public double getTop(){
        return bbox.top;
    }
    public double getBottom(){
        return bbox.bottom;
    }

    @Override
    public boolean contains(double x, double y) {
        boolean result=false;
        if(bbox.left<=x&&bbox.top<=y&&bbox.right>=x&&bbox.bottom>=y){
            result=true;
        }
        return result;
       
    }
    
    @Override
    public boolean inRectangle(double x1, double y1, double x2, double y2) {
        return bbox.left>=x1&&bbox.top>=y1&&bbox.right<=x2&&bbox.bottom<=y2;
    }

    @Override
    public void draw(GraphicsContext gc,boolean selected) {
        gc.setStroke(Color.BLACK);
        gc.strokeRect(bbox.left, bbox.top,bbox.right-bbox.left,bbox.bottom-bbox.top);
        for(int i=0;i<groups.size();i++){
            if(groups.get(i).hasChildren()){                //if is a group,go in that group and call contains method again
                groups.get(i).draw(gc,selected);
            }   
            else{
                groups.get(i).draw(gc,selected);
            }
        }
    }

    @Override
    public void move(double dx, double dy) {
        for(int i=0;i<groups.size();i++){
            if(groups.get(i).hasChildren()){                
                groups.get(i).move(dx, dy);
                
            }else{              
                groups.get(i).move(dx, dy);
                
            }
        }
        bbox.left+=dx;
        bbox.right+=dx;
        bbox.top+=dy;
        bbox.bottom+=dy;
    }

    @Override
    public void setZ(int newZ) {
        this.zOrder=newZ;
    }

    @Override
    public int getZ() {
        return this.zOrder;
    }

    
    
    public boolean hasChildren(){
        return true;
    }
    public ArrayList<Groupable> ChildrenList(){
        return groups;
    }

    @Override
    public Groupable clone() {
        ArrayList<Groupable> n= new ArrayList<Groupable>();
        for (int i=0;i< this.groups.size();i++){
                n.add(groups.get(i).clone());
        }
        Group x = new Group(n);
        return x;
    }
    
    
    
}
