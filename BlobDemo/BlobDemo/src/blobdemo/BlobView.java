/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blobdemo;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 *
 * @author gutwin
 */
public class BlobView extends Pane implements ModelListener {

    Canvas blobCanvas;
    GraphicsContext gc;
    BlobModel model;
    InteractionModel iModel;

    public BlobView(double width, double height) {
        blobCanvas = new Canvas(width, height);
        gc = blobCanvas.getGraphicsContext2D();
        getChildren().add(blobCanvas);
    }

    public void setModel(BlobModel aModel) {
        model = aModel;
    }

    public void setInteractionModel(InteractionModel anIModel) {
        iModel = anIModel;
    }

    public void draw() {
        // draw background
        if (iModel.controlDown) {
            gc.setFill(Color.GREY);
        } else {
            gc.setFill(Color.DARKGREY);
        }
        gc.fillRect(0, 0, blobCanvas.getWidth(), blobCanvas.getHeight());
        if (iModel.hasRubberband()) {
            gc.setFill(Color.YELLOW);
            gc.fillRect(iModel.rubber.left, iModel.rubber.top, iModel.rubber.width, iModel.rubber.height);
        }
        for(int i=0;i<model.entites.size();i++){
            if(iModel.selectionSet.contains(model.entites.get(i))){
                model.entites.get(i).draw(gc, true);
            }
            else{
                model.entites.get(i).draw(gc, false);
            }
        }
        // draw rubberband if exists
        


       
    }
    public void modelChanged() {
        draw();
    }
    @Override
    public void modelChanged(double dx, double dy) {
        gc.save();
        if (iModel.selectionSet != null) {
            for(int i=0;i<iModel.selectionSet.size();i++){
                Groupable b = iModel.selectionSet.get(i);
                b.move(dx, dy);
            }
            
            
        }
        draw();
    }
}
